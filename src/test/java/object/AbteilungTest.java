package object;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import object.Abteilung;

/**
 * 
 * @author Dominic Bühler
 * coverage 100%
 */



class AbteilungTest {
	@Test
	void testEmptyContructorYieldsEmptyObject() {
		Abteilung i = new Abteilung();
		assertThat("AbteilungID",i.getAbteilungid(),is(equalTo(0L)));
		assertThat("Abteilung",i.getAbteilung(),is(equalTo(null)));
		assertThat("Gueltigkeit",i.getGueltigkeit(),is(equalTo(null)));
	}
	
	@ParameterizedTest
	@CsvSource({"1,'Abteilung','1999-12-31'"})
	void testContructorAssignsAllFields(long abteilungid,String abteilung, String gueltigkeit) {
		Abteilung i = new Abteilung(abteilungid, abteilung, gueltigkeit);
		assertThat("AbteilungID",i.getAbteilungid(),equalTo(abteilungid));
		assertThat("Abteilung",i.getAbteilung(),equalTo(abteilung));
		assertThat("Gueltigkeit",i.getGueltigkeit(),equalTo(gueltigkeit));
	}
	
	
	@ParameterizedTest
	@CsvSource({"1,'Abteilung','1999-12-31'"})
	void testSetters(long abteilungid,String abteilung, String gueltigkeit) {
		Abteilung i = new Abteilung();
		i.setAbteilungid(abteilungid);
		i.setAbteilung(abteilung);
		i.setGueltigkeit(gueltigkeit);
		assertThat("Abteilungid",i.getAbteilungid(),equalTo(abteilungid));
		assertThat("Abteilung",i.getAbteilung(),equalTo(abteilung));
		assertThat("Gueltigkeit",i.getGueltigkeit(),equalTo(gueltigkeit));
	}
	
	@Test
	void testToString() {
		Abteilung i = new Abteilung();
		System.out.println(i.toString());
		assertThat("toString Mitarbeiter test",i.toString(),not(nullValue()));
	}
}
