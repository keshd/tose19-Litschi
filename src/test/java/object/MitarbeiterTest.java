package object;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import object.Mitarbeiter;

/**
 * 
 * @author Dominic Bühler
 * coverage 100%
 */

class MitarbeiterTest {
	@Test
	void testEmptyContructorYieldsEmptyObject() {
		Mitarbeiter i = new Mitarbeiter();
		assertThat("MitarbeiterID",i.getMitarbeiterid(),is(equalTo(0L)));
		assertThat("Vorname",i.getVorname(),is(equalTo(null)));
		assertThat("Nachname",i.getNachname(),is(equalTo(null)));
		assertThat("Gueltigkeit",i.getGueltigkeit(),is(equalTo(false)));
		assertThat("AbteilungsID",i.getAbteilungsid(), is(equalTo(0L)));
		assertThat("Abteilungsname",i.getAbteilungsname(),is(equalTo(null)));
		assertThat("Geburtsdatum",i.getGeburtsdatum(),is(equalTo(null)));
	}
	
	@ParameterizedTest
	@CsvSource({"1,'Vorname','Nachname',1,'Abteilungsname',1,'1999-12-31'"})
	void testContructorAssignsAllFields(long mitarbeiterid,String vorname, String nachname, Boolean gueltigkeit, String abteilungsname, long abteilungsid, String geburtsdatum) {
		Mitarbeiter i = new Mitarbeiter(mitarbeiterid, vorname, nachname, gueltigkeit, abteilungsname, abteilungsid, geburtsdatum);
		assertThat("MitarbeiterID",i.getMitarbeiterid(),equalTo(mitarbeiterid));
		assertThat("Vorame",i.getVorname(),equalTo(vorname));
		assertThat("Nachname",i.getNachname(),equalTo(nachname));
		assertThat("Gueltigkeit",i.getGueltigkeit(),equalTo(gueltigkeit));
		assertThat("AbteilungsID",i.getAbteilungsid(),equalTo(abteilungsid));
		assertThat("Abteilungsname",i.getAbteilungsname(),equalTo(abteilungsname));
		assertThat("Geburtsdatum",i.getGeburtsdatum(),equalTo(geburtsdatum));
	}
	
	@ParameterizedTest
	@CsvSource({"1,'Vorname','Nachname',1,1,'1999-12-31',1"})
	void testContructorAssignsAllFields(long mitarbeiterid, String vorname, String nachname, boolean gueltigkeit, long abteilungsid, String geburtsdatum) {
		Mitarbeiter i = new Mitarbeiter(mitarbeiterid, vorname, nachname, gueltigkeit, abteilungsid, geburtsdatum);
		assertThat("MitarbeiterID",i.getMitarbeiterid(),equalTo(mitarbeiterid));
		assertThat("Vorame",i.getVorname(),equalTo(vorname));
		assertThat("Nachname",i.getNachname(),equalTo(nachname));
		assertThat("Gueltigkeit",i.getGueltigkeit(),equalTo(gueltigkeit));
		assertThat("AbteilungsID",i.getAbteilungsid(),equalTo(abteilungsid));
		assertThat("Geburtsdatum",i.getGeburtsdatum(),equalTo(geburtsdatum));
	}
	
	@ParameterizedTest
	@CsvSource({"1,'Vorname','Nachname',1,'Abteilungsname',1,'1999-01-01'"})
	void testSetters(long mitarbeiterid,String vorname, String nachname, Boolean gueltigkeit, String abteilungsname, long abteilungsid, String geburtsdatum) {
		Mitarbeiter i = new Mitarbeiter();
		i.setMitarbeiterid(mitarbeiterid);
		i.setVorname(vorname);
		i.setNachname(nachname);
		i.setGueltigkeit(gueltigkeit);
		i.setAbteilungsname(abteilungsname);
		i.setAbteilungsid(abteilungsid);
		i.setGeburtsdatum(geburtsdatum);
		assertThat("MitarbeiterID",i.getMitarbeiterid(),equalTo(mitarbeiterid));
		assertThat("Vorname",i.getVorname(),equalTo(vorname));
		assertThat("Nachname",i.getNachname(),equalTo(nachname));
		assertThat("Gueltigkeit",i.getGueltigkeit(),equalTo(gueltigkeit));
		assertThat("Abteilungsname",i.getAbteilungsname(),equalTo(abteilungsname));
		assertThat("Abteilungsid",i.getAbteilungsid(),equalTo(abteilungsid));
		assertThat("Geburtsdatum",i.getGeburtsdatum(),equalTo(geburtsdatum));
	}
	
	@Test
	void testToString() {
		Mitarbeiter i = new Mitarbeiter();
		System.out.println(i.toString());
		assertThat("toString Mitarbeiter test",i.toString(),not(nullValue()));
	}
}
