package object;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import object.Kontaktperson;

/**
 * 
 * @author Dominic Bühler
 * coverage 100%
 */


class KontaktpersonTest {
	@Test
	void testEmptyContructorYieldsEmptyObject() {
		Kontaktperson i = new Kontaktperson();
		assertThat("kontaktpersonid",i.getKontaktpersonid(),is(equalTo(0L)));
		assertThat("kontaktperson",i.getKontaktperson(),is(equalTo(null)));
		assertThat("telefonnummer",i.getTelefonnummer(),is(equalTo(null)));
		assertThat("mitarbeiterid",i.getMitarbeiterid(),is(equalTo(0L)));
		assertThat("mitarbeitername",i.getMitarbeitername(),is(equalTo(null)));
	}
	
	@ParameterizedTest
	@CsvSource({"1,'Kontaktperson','Telefonnummer',1,'Mitarbeitername'"})
	void testContructorAssignsAllFields(long kontaktpersonid,String kontaktperson, String telefonnummer, long mitarbeiterid, String mitarbeitername) {
		Kontaktperson i = new Kontaktperson(kontaktpersonid, kontaktperson, telefonnummer, mitarbeiterid, mitarbeitername);
		assertThat("kontaktpersonid",i.getKontaktpersonid(),is(kontaktpersonid));
		assertThat("kontaktperson",i.getKontaktperson(),is(kontaktperson));
		assertThat("telefonnummer",i.getTelefonnummer(),is(telefonnummer));
		assertThat("mitarbeiterid",i.getMitarbeiterid(),is(mitarbeiterid));
		assertThat("mitarbeitername",i.getMitarbeitername(),is(mitarbeitername));
	}
	
	
	@ParameterizedTest
	@CsvSource({"1,'Kontaktperson','Telefonnummer',1,'Mitarbeitername'"})
	void testSetters(long kontaktpersonid,String kontaktperson, String telefonnummer, long mitarbeiterid, String mitarbeitername) {
		Kontaktperson i = new Kontaktperson();
		i.setKontaktpersonid(kontaktpersonid);
		i.setKontaktperson(kontaktperson);
		i.setTelefonnummer(telefonnummer);
		i.setMitarbeiterid(mitarbeiterid);
		i.setMitarbeitername(mitarbeitername);
		assertThat("KontaktpersonID",i.getKontaktpersonid(),equalTo(kontaktpersonid));
		assertThat("Kontaktperson",i.getKontaktperson(),equalTo(kontaktperson));
		assertThat("Telefonnummer",i.getTelefonnummer(),equalTo(telefonnummer));
		assertThat("MitarbeiterID",i.getMitarbeiterid(),equalTo(mitarbeiterid));
		assertThat("Mitarbeitername",i.getMitarbeitername(),equalTo(mitarbeitername));
		
	}
	
	@Test
	void testToString() {
		Kontaktperson i = new Kontaktperson();
		System.out.println(i.toString());
		assertThat("toString Mitarbeiter test",i.toString(),not(nullValue()));
	}
}
