package object;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import object.Status;

/**
 * 
 * @author Dominic Bühler
 * coverage 100%
 */
	

class StatusTest {
	@Test
	void testEmptyContructorYieldsEmptyObject() {
		Status i = new Status();
		assertThat("StatusID",i.getStatusid(),is(equalTo(0L)));
		assertThat("Name",i.getName(),is(equalTo(null)));
	}
	
	@ParameterizedTest
	@CsvSource({"1,'Name'"})
	void testContructorAssignsAllFields(long statusid,String name) {
		Status i = new Status(statusid, name);
		assertThat("StatusID",i.getStatusid(),equalTo(statusid));
		assertThat("Titel",i.getName(),equalTo(name));
	}
	
	
	@ParameterizedTest
	@CsvSource({"1,'Name'"})
	void testSetters(long statusid, String name) {
		Status i = new Status();
		i.setStatusid(statusid);
		i.setName(name);
		
		assertThat("StatusID",i.getStatusid(),equalTo(statusid));
		assertThat("Name",i.getName(),equalTo(name));
	}
	
	@Test
	void testToString() {
		Status i = new Status();
		System.out.println(i.toString());
		assertThat("toString Status test",i.toString(),not(nullValue()));
	}
}
