package object;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import object.Task;

/**
 * 
 * @author Dominic Bühler
 * coverage 100%
 */

class TaskTest {
	@Test
	void testEmptyContructorYieldsEmptyObject() {
		Task i = new Task();
		assertThat("TaskID",i.getTaskid(),is(equalTo(0L)));
		assertThat("Titel",i.getTitel(),is(equalTo(null)));
		assertThat("Beschreibung",i.getBeschreibung(),is(equalTo(null)));
		assertThat("MitarbeiterID",i.getMitarbeiterid(),is(equalTo(0L)));
		assertThat("Mitarbeitername",i.getMitarbeitername(), is(equalTo(null)));
		assertThat("Start",i.getStart(),is(equalTo(null)));
		assertThat("Ende",i.getEnde(),is(equalTo(null)));
		assertThat("Status",i.getStatus(),is(equalTo(0)));
		assertThat("Statustext",i.getStatus(),is(equalTo(0)));
	}
	
	@ParameterizedTest
	@CsvSource({"1,'Titel','Beschreibung','Mitarbeitername',1,'1900-01-01','1999-12-31',1,'erfasst'"})
	void testContructorAssignsAllFields(long taskid,String titel, String beschreibung, String mitarbeitername, long mitarbeiterid, String start, String ende, int status, String statustext) {
		Task i = new Task(taskid, titel, beschreibung, mitarbeitername, mitarbeiterid, start, ende, status, statustext);
		assertThat("TaskID",i.getTaskid(),equalTo(taskid));
		assertThat("Titel",i.getTitel(),equalTo(titel));
		assertThat("Beschreibung",i.getBeschreibung(),equalTo(beschreibung));
		assertThat("Mitarbeitername",i.getMitarbeitername(),equalTo(mitarbeitername));
		assertThat("MitarbeiterID",i.getMitarbeiterid(),equalTo(mitarbeiterid));
		assertThat("Start",i.getStart(),equalTo(start));
		assertThat("Ende",i.getEnde(),equalTo(ende));
		assertThat("Status",i.getStatus(),equalTo(status));
		assertThat("Statustext",i.getStatustext(),equalTo(statustext));
	}
	
	@ParameterizedTest
	@CsvSource({"1,'Titel','Beschreibung',1,'1900-01-01','1999-12-31',1"})
	void testContructorAssignsAllFields(long taskid,String titel, String beschreibung, long mitarbeiterid, String start, String ende, int status) {
		Task i = new Task(taskid, titel, beschreibung, mitarbeiterid, start, ende, status);
		assertThat("TaskID",i.getTaskid(),equalTo(taskid));
		assertThat("Titel",i.getTitel(),equalTo(titel));
		assertThat("Beschreibung",i.getBeschreibung(),equalTo(beschreibung));
		assertThat("MitarbeiterID",i.getMitarbeiterid(),equalTo(mitarbeiterid));
		assertThat("Start",i.getStart(),equalTo(start));
		assertThat("Ende",i.getEnde(),equalTo(ende));
		assertThat("Status",i.getStatus(),equalTo(status));
	}
	
	@ParameterizedTest
	@CsvSource({"1,'Titel','Beschreibung','Mitarbeitername',1,'1900-01-01','1999-12-31',1,'erfasst'"})
	void testSetters(long taskid, String titel, String beschreibung, String mitarbeitername, long mitarbeiterid, String start, String ende, int status, String statustext) {
		Task i = new Task();
		i.setTaskid(taskid);
		i.setTitel(titel);
		i.setBeschreibung(beschreibung);
		i.setMitarbeitername(mitarbeitername);
		i.setMitarbeiterid(mitarbeiterid);
		i.setStart(start);
		i.setEnde(ende);
		i.setStatus(status);
		i.setStatustext(statustext);
		assertThat("MitarbeiterID",i.getTaskid(),equalTo(taskid));
		assertThat("Titel",i.getTitel(),equalTo(titel));
		assertThat("Beschreibung",i.getBeschreibung(),equalTo(beschreibung));
		assertThat("Mitarbeitername",i.getMitarbeitername(),equalTo(mitarbeitername));
		assertThat("MitarbeiterID",i.getMitarbeiterid(),equalTo(mitarbeiterid));
		assertThat("Start",i.getStart(),equalTo(start));
		assertThat("Ende",i.getEnde(),equalTo(ende));
		assertThat("Status",i.getStatus(),equalTo(status));
		assertThat("Statustext",i.getStatustext(),equalTo(statustext));
	}
	
	@Test
	void testToString() {
		Task i = new Task();
		System.out.println(i.toString());
		assertThat("toString Task test",i.toString(),not(nullValue()));
	}
}
