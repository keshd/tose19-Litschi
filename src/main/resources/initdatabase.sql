drop table if exists Abteilungsliste;
drop table if exists Mitarbeiter;
drop table if exists Notfallliste;
drop table if exists status;
drop table if exists task;


CREATE TABLE IF NOT EXISTS Status (
    PK_StatusID LONG PRIMARY KEY,
    Name VARCHAR(200) NOT NULL
);

CREATE TABLE IF NOT EXISTS Abteilungsliste (
    PK_AbteilungsID LONG IDENTITY PRIMARY KEY,
	Abteilung VARCHAR(100) NOT NULL,
    Gueltigkeit date NOT NULL,
);

CREATE TABLE IF NOT EXISTS Notfallliste (
    PK_NotfallID LONG IDENTITY PRIMARY KEY,
    Kontaktperson VARCHAR(200) NOT NULL,
	Telefonnummer VARCHAR(100),
	FK_MitarbeiterID LONG
);

CREATE TABLE IF NOT EXISTS Mitarbeiter (
    PK_MitarbeiterID LONG IDENTITY PRIMARY KEY,
	Vorname VARCHAR(100) NOT NULL,
	Nachname VARCHAR(100) NOT NULL,
	Geburtsdatum date NOT NULL,
	Gueltigkeit BOOLEAN DEFAULT True NOT NULL,
	FK_AbteilungsID LONG	
);
ALTER TABLE Mitarbeiter
ADD FOREIGN KEY (FK_AbteilungsID) REFERENCES Abteilungsliste(PK_AbteilungsID) ON DELETE SET NULL;
ALTER TABLE Notfallliste
ADD FOREIGN KEY (FK_MitarbeiterID) REFERENCES Mitarbeiter(PK_MitarbeiterID) ON DELETE SET NULL;

CREATE TABLE IF NOT EXISTS Task (
    PK_TaskID LONG IDENTITY PRIMARY KEY,
    Titel VARCHAR(200) NOT NULL,
    Beschreibung VARCHAR(200) NOT NULL,
    Start date NOT NULL,
    Ende date NOT NULL,
    FK_StatusID LONG,
	FK_MitarbeiterID LONG
);
ALTER TABLE mitarbeiter
ADD FOREIGN KEY (FK_AbteilungsID) REFERENCES Abteilungsliste(PK_AbteilungsID) ON DELETE SET NULL;
ALTER TABLE task
ADD FOREIGN KEY (FK_mitarbeiterid) REFERENCES Mitarbeiter(PK_MitarbeiterID) ON DELETE SET NULL;
Alter Table task
ADD Foreign KEY (FK_StatusID) REFERENCES Status(PK_StatusID) ON DELETE SET NULL;

/*******************************************************************************************
Test Fälle (für Demo)
*******************************************************************************************/

INSERT INTO Abteilungsliste (PK_AbteilungsID, Abteilung, Gueltigkeit) VALUES (1, 'Linux', '2019-01-02');
INSERT INTO Abteilungsliste (PK_AbteilungsID, Abteilung, Gueltigkeit) VALUES (2, 'Windows', '2019-01-02');
INSERT INTO Abteilungsliste (PK_AbteilungsID, Abteilung, Gueltigkeit) VALUES (3, 'Oracle', '2019-01-02');
INSERT INTO Abteilungsliste (PK_AbteilungsID, Abteilung, Gueltigkeit) VALUES (4, 'Middleware', '2019-02-02');
INSERT INTO Abteilungsliste (PK_AbteilungsID, Abteilung, Gueltigkeit) VALUES (5, 'VMware', '2019-03-02');
INSERT INTO Abteilungsliste (PK_AbteilungsID, Abteilung, Gueltigkeit) VALUES (6, 'Netzwerk', '2019-04-02');

INSERT INTO Mitarbeiter VALUES (1, 'Max', 'Mustermann', '1999-01-02', True, 1); 
INSERT INTO Mitarbeiter VALUES (2, 'Peter', 'Lustig', '1999-01-02', True, 1); 
INSERT INTO Mitarbeiter VALUES (3, 'Franz', 'Ferdinand', '1999-01-02', True, 1); 
INSERT INTO Mitarbeiter VALUES (4, 'Kaspar', 'Schweizer', '1999-01-02', True, 1); 
INSERT INTO Mitarbeiter VALUES (5, 'Franziska', 'Schulz', '1999-01-02', True, 1); 
INSERT INTO Mitarbeiter VALUES (6, 'Deven', 'Keshwala', '1993-01-02', True, 1); 
INSERT INTO Mitarbeiter VALUES (7, 'Domninic', 'Bühler', '1893-01-02', True, 1);
INSERT INTO Mitarbeiter VALUES (8, 'Deniz', 'Klassenchef', '1993-01-06', True, 1);
INSERT INTO Mitarbeiter VALUES (9, 'Simon', 'Test', '1993-01-06', True, 1);

INSERT INTO Status (PK_StatusID, Name) VALUES (1, 'Erfasst');
INSERT INTO Status (PK_StatusID, Name) VALUES (2, 'in Arbeit');
INSERT INTO Status (PK_StatusID, Name) VALUES (3, 'Erledigt');
INSERT INTO Status (PK_StatusID, Name) VALUES (4, 'Wartend');
INSERT INTO Status (PK_StatusID, Name) VALUES (5, 'in Prüfung');

INSERT INTO Task (PK_TaskID, Titel, Beschreibung, Start, Ende, FK_StatusID, FK_MitarbeiterID) VALUES (1, 'Produktion', 'Präsentation vorbereiten','2019-01-01', '2019-02-28', 1, 1);
INSERT INTO Task (PK_TaskID, Titel, Beschreibung, Start, Ende, FK_StatusID, FK_MitarbeiterID) VALUES (2, 'Produktion', 'Dokumente erarbeiten','2019-02-01', '2019-03-31', 2, 2);
INSERT INTO Task (PK_TaskID, Titel, Beschreibung, Start, Ende, FK_StatusID, FK_MitarbeiterID) VALUES (3, 'Release', 'Planung erstellen','2019-02-01', '2019-03-31', 3, 3);
INSERT INTO Task (PK_TaskID, Titel, Beschreibung, Start, Ende, FK_StatusID, FK_MitarbeiterID) VALUES (4, 'Mitarbeiterinformation', 'Mitarbeiter einladen','2019-01-01', '2019-03-31', 4, 4);
INSERT INTO Task (PK_TaskID, Titel, Beschreibung, Start, Ende, FK_StatusID, FK_MitarbeiterID) VALUES (5, 'Mitarbeiterinformation', 'Mitarbeiter Gespräch führen','2019-01-01', '2019-03-31', 4, 5);
INSERT INTO Task (PK_TaskID, Titel, Beschreibung, Start, Ende, FK_StatusID, FK_MitarbeiterID) VALUES (6, 'Finanzen', 'Audits durchführen','2019-03-01', '2019-03-31', 3, 4);
INSERT INTO Task (PK_TaskID, Titel, Beschreibung, Start, Ende, FK_StatusID, FK_MitarbeiterID) VALUES (7, 'Finanzen', 'Jahresabschluss','2019-03-01', '2019-03-31', 1, 3);
INSERT INTO Task (PK_TaskID, Titel, Beschreibung, Start, Ende, FK_StatusID, FK_MitarbeiterID) VALUES (8, 'Server', 'Server verkabeln','2019-01-01', '2019-02-28', 1, 1);
INSERT INTO Task (PK_TaskID, Titel, Beschreibung, Start, Ende, FK_StatusID, FK_MitarbeiterID) VALUES (9, 'Server', 'Server aufsetzen','2019-02-01', '2019-03-31', 2, 2);
INSERT INTO Task (PK_TaskID, Titel, Beschreibung, Start, Ende, FK_StatusID, FK_MitarbeiterID) VALUES (10, 'Datenbank', 'Datenbank installieren','2019-02-01', '2019-03-31', 3, 3);
INSERT INTO Task (PK_TaskID, Titel, Beschreibung, Start, Ende, FK_StatusID, FK_MitarbeiterID) VALUES (11, 'Firewall', 'Firewallports öffnen','2019-01-01', '2019-03-31', 4, 4);
INSERT INTO Task (PK_TaskID, Titel, Beschreibung, Start, Ende, FK_StatusID, FK_MitarbeiterID) VALUES (12, 'Server', 'Dekomissionierung','2019-01-01', '2019-03-31', 4, 5);
INSERT INTO Task (PK_TaskID, Titel, Beschreibung, Start, Ende, FK_StatusID, FK_MitarbeiterID) VALUES (13, 'Windows', 'Updates einspielen','2019-03-01', '2019-03-31', 3, 4);
INSERT INTO Task (PK_TaskID, Titel, Beschreibung, Start, Ende, FK_StatusID, FK_MitarbeiterID) VALUES (14, 'Windows', 'Tests durchführen','2019-03-01', '2019-03-31', 1, 3);
INSERT INTO Task (PK_TaskID, Titel, Beschreibung, Start, Ende, FK_StatusID, FK_MitarbeiterID) VALUES (15, 'Projekt xy', 'Anforderungen definieren','2019-01-01', '2019-02-28', 1, 1);
INSERT INTO Task (PK_TaskID, Titel, Beschreibung, Start, Ende, FK_StatusID, FK_MitarbeiterID) VALUES (16, 'Projekt xy', 'Lastenheft erstellen','2019-02-01', '2019-03-31', 2, 2);
INSERT INTO Task (PK_TaskID, Titel, Beschreibung, Start, Ende, FK_StatusID, FK_MitarbeiterID) VALUES (17, 'Teamlist', 'Testfälle definieren','2019-02-01', '2019-03-31', 3, 3);
INSERT INTO Task (PK_TaskID, Titel, Beschreibung, Start, Ende, FK_StatusID, FK_MitarbeiterID) VALUES (18, 'Teamlist', 'Trello Board aktualisieren','2019-01-01', '2019-03-31', 4, 4);
INSERT INTO Task (PK_TaskID, Titel, Beschreibung, Start, Ende, FK_StatusID, FK_MitarbeiterID) VALUES (19, 'Teamlist', 'Sprintplanning durchführen','2019-01-01', '2019-03-31', 4, 5);
INSERT INTO Task (PK_TaskID, Titel, Beschreibung, Start, Ende, FK_StatusID, FK_MitarbeiterID) VALUES (20, 'Teamlist', 'Sprint Retrospektive druchführen','2019-03-01', '2019-03-31', 3, 4);
INSERT INTO Task (PK_TaskID, Titel, Beschreibung, Start, Ende, FK_StatusID, FK_MitarbeiterID) VALUES (21, 'Produktion', 'Berechtigungen für MA xy anpassen','2019-03-01', '2019-03-31', 1, 3);

INSERT INTO Notfallliste (PK_Notfallid, Kontaktperson, Telefonnummer, FK_Mitarbeiterid) VALUES (1, 'Hans', '+41795194735', 1);
INSERT INTO Notfallliste (PK_Notfallid, Kontaktperson, Telefonnummer, FK_Mitarbeiterid) VALUES (2, 'Fritz', '+41795194735', 3);
INSERT INTO Notfallliste (PK_Notfallid, Kontaktperson, Telefonnummer, FK_Mitarbeiterid) VALUES (3, 'Deven', '+41795194735', 2);
INSERT INTO Notfallliste (PK_Notfallid, Kontaktperson, Telefonnummer, FK_Mitarbeiterid) VALUES (4, 'Keshwala', '+41795194735', 4);
INSERT INTO Notfallliste (PK_Notfallid, Kontaktperson, Telefonnummer, FK_Mitarbeiterid) VALUES (5, 'Simon', '+41795194735', 5);
INSERT INTO Notfallliste (PK_Notfallid, Kontaktperson, Telefonnummer, FK_Mitarbeiterid) VALUES (6, 'Richard', '+41795194735', 6);
