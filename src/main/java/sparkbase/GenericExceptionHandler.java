package sparkbase;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import spark.ExceptionHandler;
import spark.Request;
import spark.Response;
import sparklist.SparkListServer;

@SuppressWarnings("rawtypes")
public class GenericExceptionHandler implements ExceptionHandler {

    final static Logger log = LoggerFactory.getLogger(SparkListServer.class);

	@Override
	public void handle(Exception exception, Request request, Response response) {
	  String msg = String.format("EXCEPTION while processing %s : %s - %s", 
			  request.url(),
			  exception.getClass().getSimpleName(),
			  exception.getLocalizedMessage());
	  log.error(msg,exception);
	  
	  response.status(500);
	  response.type("text/plain");
	  response.body(msg);
	}

}
