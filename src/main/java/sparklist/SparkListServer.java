package sparklist;

import static spark.Spark.get;
import static spark.Spark.post;

import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sparkbase.H2SparkApp;
import sparkbase.UTF8ThymeleafTemplateEngine;
import webcontroller.*;

public class SparkListServer extends H2SparkApp {

	final static Logger log = LoggerFactory.getLogger(SparkListServer.class);

	public static void main(String[] args) {
		
		for(Entry<Object, Object> property: System.getProperties().entrySet()) {
			log.debug(String.format("Property %s : %s", property.getKey(),property.getValue()));
		}

		SparkListServer server = new SparkListServer();
		server.configure();
		server.run();
	}
	
	@Override
	protected void doConfigureHttpHandlers() {
		
		//Root Controler
		get("/", new RootController(), new UTF8ThymeleafTemplateEngine());
		
		//Task controlers
		get("/task", new TaskListController(), new UTF8ThymeleafTemplateEngine());
		get("/task/edit", new TaskEditController(), new UTF8ThymeleafTemplateEngine());
		get("/task/create", new TaskCreateController(), new UTF8ThymeleafTemplateEngine());
		post("/task/update", new TaskUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/task/delete", new TaskDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/task/new", new TaskNewController(), new UTF8ThymeleafTemplateEngine());
		
		//Mitarbeiter controllers
		get("/mitarbeiter", new MitarbeiterListController(), new UTF8ThymeleafTemplateEngine());
		get("/mitarbeiter/edit", new MitarbeiterEditController(), new UTF8ThymeleafTemplateEngine());
		get("/mitarbeiter/create", new MitarbeiterCreateController(), new UTF8ThymeleafTemplateEngine());
		post("/mitarbeiter/update", new MitarbeiterUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/mitarbeiter/delete", new MitarbeiterDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/mitarbeiter/new", new MitarbeiterNewController(), new UTF8ThymeleafTemplateEngine());
		
		//Abteilung controllers
		get("/abteilung", new AbteilungListController(), new UTF8ThymeleafTemplateEngine());
		get("/abteilung/edit", new AbteilungEditController(), new UTF8ThymeleafTemplateEngine());
		get("/abteilung/create", new AbteilungCreateController(), new UTF8ThymeleafTemplateEngine());
		post("/abteilung/update", new AbteilungUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/abteilung/delete", new AbteilungDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/abteilung/new", new AbteilungNewController(), new UTF8ThymeleafTemplateEngine());

		//Notfallliste (Kontaktperson) controllers
		get("/notfallliste", new KontaktpersonListController(), new UTF8ThymeleafTemplateEngine());
		get("/notfallliste/edit", new KontaktpersonEditController(), new UTF8ThymeleafTemplateEngine());
		get("/notfallliste/create", new KontaktpersonCreateController(), new UTF8ThymeleafTemplateEngine());
		post("/notfallliste/update", new KontaktpersonUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/notfallliste/delete", new KontaktpersonDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/notfallliste/new", new KontaktpersonNewController(), new UTF8ThymeleafTemplateEngine());
	}

}
