package repository;

import static repository.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import object.Mitarbeiter;


/**
 * Repository für alle Mitarbeiter. 
 * Hier werden alle Funktionen für die DB-Operationen zu Mitarbeiter implementiert
 * @author Dominic Bühler
 * @author Stéphane Krebs
 */

public class MitarbeiterRepository {
	
	private final Logger log = LoggerFactory.getLogger(MitarbeiterRepository.class);
	

	/**
	 * Liefert alle Mitarbeiter in der Datenbank
	 * @return Collection aller Mitarbeiter
	 */
	public Collection<Mitarbeiter> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select mitarbeiter.PK_mitarbeiterid, mitarbeiter.vorname, mitarbeiter.nachname, mitarbeiter.gueltigkeit, case when abteilungsliste.abteilung is null then '' else abteilungsliste.abteilung end as abteilungsname, abteilungsliste.PK_AbteilungsID, mitarbeiter.geburtsdatum"
														+ " from Mitarbeiter"
														+ " left join Abteilungsliste on mitarbeiter.FK_abteilungsid = abteilungsliste.PK_abteilungsid");
			ResultSet rs = stmt.executeQuery();
			return mapMitarbeiter(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all Mitarbeiter. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert den Mitarbeiter mit der übergebenen Id
	 * @param id id des Mitarbeiters
	 * @return Mitarbeiter oder NULL
	 */
	public Mitarbeiter getById(long id) {
		log.trace("getById " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select mitarbeiter.PK_mitarbeiterid, mitarbeiter.vorname, mitarbeiter.nachname, mitarbeiter.gueltigkeit, abteilungsliste.abteilung as abteilungsname, abteilungsliste.PK_AbteilungsID, mitarbeiter.geburtsdatum"
														+ " from Mitarbeiter"
														+ " left join Abteilungsliste on mitarbeiter.FK_abteilungsid = abteilungsliste.PK_abteilungsid"
														+ " where mitarbeiter.PK_mitarbeiterid=?");

			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapMitarbeiter(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving mitarbeiter by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert den übergebenen Mitarbeiter in der Datenbank anhand eines UPDATE.
	 * @param i
	 */
	public void save(Mitarbeiter i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update mitarbeiter set vorname=?, nachname=?, gueltigkeit=?, FK_abteilungsid=?, geburtsdatum=? where PK_mitarbeiterid=?");
			stmt.setString(1, i.getVorname());
			stmt.setString(2, i.getNachname());
			stmt.setBoolean(3, i.getGueltigkeit());
			stmt.setLong(4, i.getAbteilungsid());
			stmt.setString(5,  i.getGeburtsdatum());
			stmt.setLong(6, i.getMitarbeiterid());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating Mitarbeiter " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht den Mitarbeiter mit der angegebenen Id von der DB
	 * @param id Mitarbeiter ID
	 */
	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from Mitarbeiter where PK_mitarbeiterid=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing Mitarbeiter by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert den angegebenen Mitarbeiter in der DB. INSERT.
	 * @param i neu zu erstellendes Mitarbeiter
	 * @return Liefert die von der DB generierte id des neuen Mitarbeiters zurück
	 */
	public long insert(Mitarbeiter i) {
		
		log.trace("insert " + i);

		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into Mitarbeiter (vorname, nachname, geburtsdatum, gueltigkeit, FK_Abteilungsid) values (?,?,?,?,?)");
			stmt.setString(1, i.getVorname());
			stmt.setString(2, i.getNachname());
			stmt.setString(3, i.getGeburtsdatum());
			stmt.setBoolean(4, i.getGueltigkeit());
			stmt.setLong(5, i.getAbteilungsid());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating Mitarbeiter " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Mitarbeiter-Objekte
	 * @throws SQLException 
	 *
	 */
	private static Collection<Mitarbeiter> mapMitarbeiter(ResultSet rs) throws SQLException 
	{
		LinkedList<Mitarbeiter> list = new LinkedList<Mitarbeiter>();
		while(rs.next())
		{
			Mitarbeiter i = new Mitarbeiter(rs.getLong("PK_mitarbeiterid"),rs.getString("vorname"),rs.getString("nachname"),rs.getBoolean("gueltigkeit"),rs.getString("abteilungsname"),rs.getLong("PK_AbteilungsID"),rs.getString("geburtsdatum"));
			list.add(i);
		}
		return list;
	}

}
