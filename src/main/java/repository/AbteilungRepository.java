package repository;

import static repository.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import object.Abteilung;


/**
 * Repository für alle Abteilungen. 
 * Hier werden alle Funktionen für die DB-Operationen zu Abteilung implementiert
 * 
 */


public class AbteilungRepository {
	
	private final Logger log = LoggerFactory.getLogger(AbteilungRepository.class);
	

	/**
	 * Liefert alle Abteilungen in der Datenbank
	 * @return Collection aller Abteilungen
	 */
	
	public Collection<Abteilung> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select PK_abteilungsID, Abteilung, Gueltigkeit from Abteilungsliste");														 
			ResultSet rs = stmt.executeQuery();
			return mapAbteilung(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all abteilung. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	/**
	 * Liefert die Abteilung mit der übergebenen Id
	 * @param id ID der Abteilung
	 * @return Abteilungid oder NULL
	 */
	public Abteilung getById(long id) {
		log.trace("getById " + id);
		
		//TODO: There is an issue with this repository method. Find and fix it!
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select PK_abteilungsID, Abteilung, Gueltigkeit from Abteilungsliste where PK_AbteilungsID=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapAbteilung(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving abteilung by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert die übergebene Abteilung in der Datenbank anhand eines UPDATE
	 * @param i
	 */
	public void save(Abteilung i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update Abteilungsliste set abteilung=?, Gueltigkeit=? where PK_AbteilungsID=?");
			stmt.setString(1, i.getAbteilung());
			stmt.setString(2, i.getGueltigkeit());
			stmt.setLong(3, i.getAbteilungid());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating abteilung " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht die Abteilung mit der angegebenen Id von der DB
	 * @param id abteilung ID
	 */
	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from Abteilungsliste where PK_AbteilungsID=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing Abteilung by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Speichert die angegebene Abteilung in der DB. INSERT.
	 * @param i neu zu erstellendes abteilung
	 * @return Liefert die von der DB generierte id der neuenen Abteilung zurück
	 */
	public long insert(Abteilung i) {
		
		log.trace("insert " + i);

		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into Abteilungsliste (abteilung, gueltigkeit) values (?,?)");
			stmt.setString(1, i.getAbteilung());
			stmt.setString(2, i.getGueltigkeit());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating abteilung " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Abteilungs-Objekte.
	 * 
	 * @throws SQLException 
	 */
	private static Collection<Abteilung> mapAbteilung(ResultSet rs) throws SQLException 
	{
		LinkedList<Abteilung> list = new LinkedList<Abteilung>();
		while(rs.next())
		{
			Abteilung i = new Abteilung(rs.getLong("PK_abteilungsID"),rs.getString("abteilung"),rs.getString("Gueltigkeit"));
			list.add(i);
		}
		return list;
	}

}
