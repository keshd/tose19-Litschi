package repository;

public class RepositoryException extends RuntimeException {
	private static final long serialVersionUID = 8276542959481808580L;

	public RepositoryException(String message) {
		super(message);
	}

}
