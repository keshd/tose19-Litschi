package repository;

import static repository.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import object.Task;


/**
 * Repository für alle Tasks. 
 * Hier werden alle Funktionen für die DB-Operationen zu Task implementiert
 * @author Stéphane Krebs
 *
 */


public class TaskRepository {
	
	private final Logger log = LoggerFactory.getLogger(TaskRepository.class);
	

	/**
	 * Liefert alle task in der Datenbank
	 * @return Collection aller Tasks
	 */
	public Collection<Task> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select task.PK_taskid, task.titel, task.beschreibung, concat(mitarbeiter.vorname,' ', mitarbeiter.nachname) as mitarbeitername, task.FK_mitarbeiterid, task.start, task.ende, task.FK_statusid, status.name as status "
														 + "from task "
														 + "inner join status on status.PK_Statusid = task.FK_Statusid "
														 + "left join mitarbeiter on mitarbeiter.PK_mitarbeiterid = task.FK_mitarbeiterid");
			ResultSet rs = stmt.executeQuery();
			return mapTasks(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all task. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}


	/**
	 * Liefert das Task mit der übergebenen Id
	 * @param id id des Task
	 * @return Task oder NULL
	 */
	public Task getById(long id) {
		log.trace("getById " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select task.PK_taskid, task.titel, task.beschreibung, concat(mitarbeiter.vorname,' ', mitarbeiter.nachname) as mitarbeitername, task.FK_mitarbeiterid, task.start, task.ende, task.FK_statusid, status.name as status "
														 + "from task "
														 + "inner join status on status.PK_Statusid = task.FK_Statusid "
														 + "left join mitarbeiter on mitarbeiter.PK_mitarbeiterid = task.FK_mitarbeiterid "
														 + "where task.PK_taskid=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapTasks(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving tasks by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert das übergebene task in der Datenbank. UPDATE.
	 * @param i
	 */
	public void save(Task i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update task set titel=?, beschreibung=?, start=?, ende=?, FK_StatusID=?, FK_mitarbeiterid=? where PK_taskid=?");
			stmt.setString(1, i.getTitel());
			stmt.setString(2, i.getBeschreibung());
			stmt.setString(3, i.getStart());
			stmt.setString(4, i.getEnde());
			stmt.setLong(5, i.getStatus());
			stmt.setLong(6, i.getMitarbeiterid());
			stmt.setLong(7, i.getTaskid());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating task " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht das Task mit der angegebenen Id von der DB
	 * @param id Task ID
	 */
	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from task where PK_taskid=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing tasks by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert das angegebene Task in der DB. INSERT.
	 * @param i neu zu erstellendes Task
	 * @return Liefert die von der DB generierte id des neuen Tasks zurück
	 */
	public long insert(Task i) {
		
		log.trace("insert " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into task (titel, beschreibung, start, ende, FK_StatusID, FK_mitarbeiterid) values (?,?,?,?,?,?)");
			stmt.setString(1, i.getTitel());
			stmt.setString(2, i.getBeschreibung());
			stmt.setString(3, i.getStart());
			stmt.setString(4, i.getEnde());
			stmt.setLong(5, i.getStatus());
			stmt.setLong(6, i.getMitarbeiterid());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating task " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Task-Objekte. Siehe getByXXX Methoden.
	 * @author Marcel Briggen
	 * @throws SQLException 
	 *
	 */
	private static Collection<Task> mapTasks(ResultSet rs) throws SQLException 
	{
		LinkedList<Task> list = new LinkedList<Task>();
		while(rs.next())
		{
			Task i = new Task(rs.getLong("PK_taskid"),rs.getString("titel"),rs.getString("beschreibung"),rs.getString("mitarbeitername"),rs.getLong("FK_mitarbeiterid"),rs.getString("start"),rs.getString("ende"), rs.getInt("FK_statusid"), rs.getString("status"));
			list.add(i);
		}
		return list;
	}

}
