package repository;

import static repository.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import object.Kontaktperson;


/**
 * Repository für alle Kontaktpersonen. 
 * Hier werden alle Funktionen für die DB-Operationen zu Kontaktperson implementiert
 *
 */


public class KontaktpersonRepository {
	
	private final Logger log = LoggerFactory.getLogger(KontaktpersonRepository.class);
	

	/**
	 * Liefert alle Kontaktpersonen in der Datenbank
	 * @return Collection aller Kontaktpersonen
	 */
	public Collection<Kontaktperson> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select notfallliste.PK_notfallID, notfallliste.Kontaktperson, notfallliste.Telefonnummer, notfallliste.FK_Mitarbeiterid, concat(mitarbeiter.vorname,' ', mitarbeiter.nachname) as mitarbeitername "
														 + "from Notfallliste "
														 + "left join mitarbeiter on mitarbeiter.PK_mitarbeiterid = notfallliste.FK_mitarbeiterid");														 
			ResultSet rs = stmt.executeQuery();
			return mapKontaktperson(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all Kontaktpersonen. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	

	/**
	 * Liefert die kontaktperson mit der übergebenen Id
	 * @param id der Kontaktperson
	 * @return Kontaktperson oder NULL
	 */
	public Kontaktperson getById(long id) {
		log.trace("getById " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select notfallliste.PK_notfallID, notfallliste.Kontaktperson, notfallliste.Telefonnummer, notfallliste.FK_Mitarbeiterid, concat(mitarbeiter.vorname,' ', mitarbeiter.nachname) as mitarbeitername "
					 									 + "from Notfallliste "
					 									 + "left join mitarbeiter on mitarbeiter.PK_mitarbeiterid = notfallliste.FK_mitarbeiterid "
					 									 + "where PK_NotfallID=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapKontaktperson(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving Kontaktperson by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert die übergebene Kontaktperson in der Datenbank anhand eines UPDATE.
	 * @param i
	 */
	public void save(Kontaktperson i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update Notfallliste set kontaktperson=?, Telefonnummer=?, FK_MitarbeiterID=? where PK_NotfallID=?");
			stmt.setString(1, i.getKontaktperson());
			stmt.setString(2, i.getTelefonnummer());
			stmt.setLong(3, i.getMitarbeiterid());
			stmt.setLong(4, i.getKontaktpersonid());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating Kontaktperson " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht die Kontaktperson mit der angegebenen Id von der DB
	 * @param id Kontaktperson ID
	 */
	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from Notfallliste where PK_NotfallID=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleting Kontaktperson by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}					

	}
	
	/**
	 * Löscht die Kontaktperson mit der angegebenen MitarbeiterId von der DB
	 * @param id Kontaktperson FK_MitarbeiterID
	 */
	public void deleteKontaktperson(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from Notfallliste where FK_MitarbeiterID=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleting Kontaktperson by mitarbeiterid " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}					

	}
	

	/**
	 * Speichert die angegebene Kontaktperson in der DB. INSERT.
	 * @param i neu zu erstellende Kontaktperson
	 * @return Liefert die von der DB generierte id der neuenen Kontaktperson zurück
	 */
	public long insert(Kontaktperson i) {
		
		log.trace("insert " + i);		
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into Notfallliste (kontaktperson, telefonnummer, FK_Mitarbeiterid) values (?,?,?)");
			stmt.setString(1, i.getKontaktperson());
			stmt.setString(2, i.getTelefonnummer());
			stmt.setLong(3, i.getMitarbeiterid());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating Kontaktperson " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Kontaktperson-Objekte.
	 * 
	 * @throws SQLException 
	 *
	 */
	private static Collection<Kontaktperson> mapKontaktperson(ResultSet rs) throws SQLException 
	{
		LinkedList<Kontaktperson> list = new LinkedList<Kontaktperson>();
		while(rs.next())
		{
			Kontaktperson i = new Kontaktperson(rs.getLong("PK_notfallID"),rs.getString("kontaktperson"),rs.getString("Telefonnummer"),rs.getLong("FK_MitarbeiterID"), rs.getString("mitarbeitername"));
			list.add(i);
		}
		return list;
	}

}
