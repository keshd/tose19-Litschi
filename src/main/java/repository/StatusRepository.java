package repository;

import static repository.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import object.Status;


/**
 * Repository für alle Status. 
 * Hier werden alle Funktionen für die DB-Operationen zu Task implementiert
 * @author Dominic Bühler
 * @author Stéphane Krebs
 */

public class StatusRepository {
	
	private final Logger log = LoggerFactory.getLogger(StatusRepository.class);
	

	/**
	 * Liefert alle Status in der Datenbank
	 * @return Collection aller Status
	 */
	public Collection<Status> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select PK_statusid, name from status");
			ResultSet rs = stmt.executeQuery();
			return mapTasks(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all task. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert den Status mit der übergebenen Id
	 * @param id id des Status
	 * @return Status oder NULL
	 */
	public Status getById(long id) {
		log.trace("getById " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select PK_statusid, name from status where PK_statusid=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapTasks(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving tasks by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Helper zum konvertieren der Resultsets in Status-Objekte..
	 * @throws SQLException 
	 *
	 */
	private static Collection<Status> mapTasks(ResultSet rs) throws SQLException 
	{
		LinkedList<Status> list = new LinkedList<Status>();
		while(rs.next())
		{
			Status i = new Status(rs.getLong("PK_statusid"),rs.getString("name"));
			list.add(i);
		}
		return list;
	}

}
