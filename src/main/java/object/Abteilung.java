package object;


/**
 * Einzelner Eintrag in der Liste mit einer eindeutigen ID, einem Abteilungsname und Gültigkeit
 * @author Dominic Bühler
 * @author Stéphane Krebs
 */
public class Abteilung {
	
	private long abteilungid;
	private String abteilung;
	private String gueltigkeit;

	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Abteilung()
	{
		
	}
	
	/**
	 * Konstruktor Abteilung
	 * @param abteilungid
	 * @param abteilung
	 * @param gueltigkeit
 	 */
	
	public Abteilung(long abteilungid, String abteilung, String gueltigkeit)
	{
		this.abteilungid = abteilungid;
		this.abteilung = abteilung;
		this.gueltigkeit = gueltigkeit;
	}
		
	public long getAbteilungid() {
		return abteilungid;
	}

	public void setAbteilungid(long abteilungid) {
		this.abteilungid = abteilungid;
	}
	
	public String getAbteilung() {
		return abteilung;
	}

	public void setAbteilung(String abteilung) {
		this.abteilung = abteilung;
	}

	public String getGueltigkeit() {
		return gueltigkeit;
	}

	public void setGueltigkeit(String gueltigkeit) {
		this.gueltigkeit = gueltigkeit;
	}

	@Override
	public String toString() {
		return String.format("Abteilung:{abteilungid: %d; abteilung: %s; gueltigkeit: %s;}", abteilungid, abteilung, gueltigkeit);
	}

}
