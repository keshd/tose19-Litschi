package object;


/**
 * Einzelner Eintrag in der Liste mit einer eindeutigen Statusid und Statusname
 * @author Dominic Bühler
 * @author Stéphane Krebs
 */
public class Status {
	
	private long statusid;
	private String name;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Status()
	{
		
	}
	
	/**
	 * Konstruktor Status
	 * @param statusid
	 * @param name
	 */
	
	public Status(long statusid, String name)
	{
		this.statusid = statusid;
		this.name = name;
	}
		
	public long getStatusid() {
		return statusid;
	}

	public void setStatusid(long statusid) {
		this.statusid = statusid;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return String.format("Status:{statusid: %d; name: %s;}", statusid, name);
	}

	
}
