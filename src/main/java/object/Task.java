package object;


/**
 * Einzelner Eintrag in der Liste mit einer eindeutigen TaskID, Titel, Beschreibung eindeutigen Mitarbeiterid, Mitarbeitername, Startdatum, Enddatum, eindeutigen StatusID, Statusbezeichnung
 * @author Dominic Bühler
 * @author Stéphane Krebs
 */
public class Task {
	
	private long taskid;
	private String titel;
	private String beschreibung;
	private long mitarbeiterid;
	private String mitarbeitername;
	private String start;
	private String ende;
	private int status;
	private String statustext;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Task()
	{
		
	}
	
	/**
	 * Konstruktor Task auflisten
	 * @param taskid
	 * @param titel
	 * @param beschreibung
	 * @param mitarbeitername
	 * @param mitarbeiterid
	 * @param start
	 * @param ende
	 * @param status
	 * @param statustext
	 */
	
	public Task(long taskid, String titel, String beschreibung, String mitarbeitername, long mitarbeiterid, String start, String ende, int status, String statustext)
	{
		this.taskid = taskid;
		this.titel = titel;
		this.beschreibung = beschreibung;
		this.mitarbeitername = mitarbeitername;
		this.mitarbeiterid = mitarbeiterid;
		this.start = start;
		this.ende = ende;
		this.status = status;
		this.statustext = statustext;
	}
		
	/**
	 * Konstruktor Task erstellen
	 * @param taskid
	 * @param titel
	 * @param beschreibung
	 * @param mitarbeiterid
	 * @param start
	 * @param ende
	 * @param status
	 */
	
	public Task(long taskid, String titel, String beschreibung, long mitarbeiterid, String start, String ende, int status)
	{
		this.taskid = taskid;
		this.titel = titel;
		this.beschreibung = beschreibung;
		this.mitarbeiterid = mitarbeiterid;
		this.start = start;
		this.ende = ende;
		this.status = status;
	}

	public long getTaskid() {
		return taskid;
	}

	public void setTaskid(long taskid) {
		this.taskid = taskid;
	}
	
	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}

	public long getMitarbeiterid() {
		return mitarbeiterid;
	}

	public void setMitarbeiterid(long mitarbeiterid) {
		this.mitarbeiterid = mitarbeiterid;
	}	
	
	public String getMitarbeitername() {
		return mitarbeitername;
	}

	public void setMitarbeitername(String mitarbeitername) {
		this.mitarbeitername = mitarbeitername;
	}	
	
	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnde() {
		return ende;
	}
	
	public void setEnde(String ende) {
		this.ende = ende;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}
	
	public String getStatustext() {
		return statustext;
	}
	
	public void setStatustext(String statustext) {
		this.statustext = statustext;
	}	
	
	@Override
	public String toString() {
		return String.format("Task:{taskid: %d; titel: %s; beschreibung: %s; mitarbeitername: %s; start: %s; ende: %s; statustext: %s;}", taskid, titel, beschreibung, mitarbeitername, start, ende, statustext);
	}

}
