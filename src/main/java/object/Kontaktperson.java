package object;


/**
 * Einzelner Eintrag in der Liste mit einer eindeutigen ID, einer Kontaktperson, Telefonnummer, eindeutigen MitarbeiterID und Mitarbeitername
 * @author Dominic Bühler
 * @author Stéphane Krebs
 */
public class Kontaktperson {
	
	private long kontaktpersonid;
	private String kontaktperson;
	private String telefonnummer;
	private long mitarbeiterid;
	private String mitarbeitername;

	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Kontaktperson()
	{
		
	}
	
	/**
	 * Konstruktor Kontaktperson
	 * @param kontaktpersonid
	 * @param kontaktperson
	 * @param telefonnummer
	 * @param mitarbeiterid
	 * @param mitarbeitername
	 */
	
	public Kontaktperson(long kontaktpersonid, String kontaktperson, String telefonnummer, long mitarbeiterid, String mitarbeitername)
	{
		this.kontaktpersonid = kontaktpersonid;
		this.kontaktperson = kontaktperson;
		this.telefonnummer = telefonnummer;
		this.mitarbeiterid = mitarbeiterid;
		this.mitarbeitername = mitarbeitername;
	}
		

	public long getKontaktpersonid() {
		return kontaktpersonid;
	}

	public void setKontaktpersonid(long kontaktpersonid) {
		this.kontaktpersonid = kontaktpersonid;
	}
	
	public String getKontaktperson() {
		return kontaktperson;
	}

	public void setKontaktperson(String kontaktperson) {
		this.kontaktperson = kontaktperson;
	}

	public String getTelefonnummer() {
		return telefonnummer;
	}

	public void setTelefonnummer(String telefonnummer) {
		this.telefonnummer = telefonnummer;
	}

	public long getMitarbeiterid() {
		return mitarbeiterid;
	}

	public void setMitarbeiterid(long mitarbeiterid) {
		this.mitarbeiterid = mitarbeiterid;
	}
	
	public String getMitarbeitername() {
		return mitarbeitername;
	}

	public void setMitarbeitername(String mitarbeitername) {
		this.mitarbeitername = mitarbeitername;
	}
	@Override
	public String toString() {
		return String.format("Kontaktperson:{kontaktpersonid: %d; kontaktperson: %s; telefonnummer: %s; mitarbeiterid: %d; mitarbeitername %s;}", kontaktpersonid, kontaktperson, telefonnummer, mitarbeiterid, mitarbeitername);
	}

}
