package object;


/**
 * Einzelner Eintrag in der Liste mit einer eindeutigen MitarbeiterID, Vorname, Nachname, Gültigkeit, eindeutigen Abteilungsid und Geburtsdatum
 * @author Dominic Bühler
 * @author Stéphane Krebs
 */
public class Mitarbeiter {
	
	private long mitarbeiterid;
	private String vorname;
	private String nachname;
	private boolean gueltigkeit;
	private long abteilungsid;
	private String abteilungsname;
	private String geburtsdatum;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Mitarbeiter()
	{
		
	}
	
	/**
	 * Konstruktor Mitarbeiter
	 * @param mitarbeiterid
	 * @param vorname
	 * @param nachname
	 * @param gueltigkeit
	 * @param abteilungsname
	 * @param abteilungsid
	 * @param geburtsdatum
	 */
	
	public Mitarbeiter(long mitarbeiterid, String vorname, String nachname, boolean gueltigkeit, String abteilungsname, long abteilungsid, String geburtsdatum)
	{
		this.mitarbeiterid = mitarbeiterid;
		this.vorname = vorname;
		this.nachname = nachname;
		this.gueltigkeit = gueltigkeit;
		this.abteilungsname = abteilungsname;
		this.abteilungsid = abteilungsid;
		this.geburtsdatum = geburtsdatum;
	}
	
	/**
	 * Konstruktor Mitarbeiter
	 * @param mitarbeiterid
	 * @param vorname
	 * @param nachname
	 * @param gueltigkeit
	 * @param abteilungsid
	 * @param geburtsdatum
	 */
	public Mitarbeiter(long mitarbeiterid, String vorname, String nachname, boolean gueltigkeit, long abteilungsid, String geburtsdatum)
	{
		this.mitarbeiterid = mitarbeiterid;
		this.vorname = vorname;
		this.nachname = nachname;
		this.gueltigkeit = gueltigkeit;
		this.abteilungsid = abteilungsid;
		this.geburtsdatum = geburtsdatum;
	}
	
	public long getMitarbeiterid() {
		return mitarbeiterid;
	}

	public void setMitarbeiterid(long mitarbeiterid) {
		this.mitarbeiterid = mitarbeiterid;
	}
	
	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	
	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public boolean getGueltigkeit() {
		return gueltigkeit;
	}

	public void setGueltigkeit(boolean gueltigkeit) {
		this.gueltigkeit = gueltigkeit;
	}

	public long getAbteilungsid() {
		return abteilungsid;
	}

	public void setAbteilungsid(long abteilungsid) {
		this.abteilungsid = abteilungsid;
	}	
	
	public String getAbteilungsname() {
		return abteilungsname;
	}

	public void setAbteilungsname(String abteilungsname) {
		this.abteilungsname = abteilungsname;
	}
	
	
	public String getGeburtsdatum() {
		return geburtsdatum;
	}

	public void setGeburtsdatum(String geburtsdatum) {
		this.geburtsdatum = geburtsdatum;
	}
	
	@Override
	public String toString() {
		return String.format("Mitarbeiter:{mitarbeiterid: %d; vorname: %s; nachname: %s; gueltigkeit: %b; abteilungsid: %d; geburtsdatum: %s;}", mitarbeiterid, vorname, nachname, gueltigkeit, abteilungsid, geburtsdatum);
	}

}
