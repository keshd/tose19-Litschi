package webcontroller;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import object.*;
import repository.KontaktpersonRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * WWW-Controller
 * Liefert unter "/notfallliste" die ganze Liste
 * 
 * @author Dominic Bühler
 * @author Stéphane Krebs
 */
public class KontaktpersonListController implements TemplateViewRoute {
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(KontaktpersonListController.class);

	KontaktpersonRepository repository = new KontaktpersonRepository();

	/**
	 *Liefert die Liste als Root-Seite "/notfallliste/list" zurück 
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		
		//Kontaktpersonen werden geladen und die Collection dann für das Template unter dem namen "list" bereitgestellt
		//Das Template muss dann auch den Namen "list" verwenden.
		HashMap<String, Collection<Kontaktperson>> model = new HashMap<String, Collection<Kontaktperson>>();
		model.put("list", repository.getAll());
		return new ModelAndView(model, "kontaktpersonListTemplate");
	}
}
