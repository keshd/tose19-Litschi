package webcontroller;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import object.Kontaktperson;
import spark.Request;

class KontaktpersonWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(KontaktpersonWebHelper.class);
	
	public static Kontaktperson kontaktpersonFromWeb(Request request)
	{
		return new Kontaktperson(
				Long.parseLong(request.queryParams("kontaktpersonDetail.kontaktpersonid")),
				request.queryParams("kontaktpersonDetail.kontaktperson"),
				request.queryParams("kontaktpersonDetail.telefonnummer"),
				Long.parseLong(request.queryParams("kontaktpersonDetail.mitarbeiterid")),
				request.queryParams("kontaktpersonDetail.mitarbeitername"));
	}

}
