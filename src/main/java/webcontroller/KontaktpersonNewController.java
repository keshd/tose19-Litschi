package webcontroller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import object.Kontaktperson;
import repository.KontaktpersonRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelne Kontaktpersonenen
 * @author Dominic Bühler
 * @author Stéphane Krebs
 */

public class KontaktpersonNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(KontaktpersonNewController.class);
		
	private KontaktpersonRepository kontaktpersonRepo = new KontaktpersonRepository();
	
	/**
	 * Erstellt ein neuen Mitarbeiter in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /Kontaktperson&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /notfallliste/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Kontaktperson kontaktpersonDetail = KontaktpersonWebHelper.kontaktpersonFromWeb(request);
		log.trace("POST /notfallliste/new mit kontaktpersonDetail " + kontaktpersonDetail);
		
		//insert gibt die von der DB erstellte id zurück.
		long id = kontaktpersonRepo.insert(kontaktpersonDetail);

		System.out.println("Neue Kontaktperson mit der ID = "+id+" erstellt");
		
		//die neue Id wird dem Redirect als Parameter nicht benötigt
		//der redirect erfolgt dann auf /notfallliste
		response.redirect("/notfallliste");
		return null;
	}
}


