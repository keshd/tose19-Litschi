package webcontroller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import repository.KontaktpersonRepository;
import repository.MitarbeiterRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Tasks
 * @author Dominic Bühler
 * @author Stéphane Krebs
 */

public class MitarbeiterDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(MitarbeiterDeleteController.class);
		
	private MitarbeiterRepository mitarbeiterRepo = new MitarbeiterRepository();
	private KontaktpersonRepository kontaktpersonRepo = new KontaktpersonRepository();

	/**
	 * Löscht den Mitarbeiter mit der übergebenen id in der Datenbank
	 * /mitarbeiter/delete&id=987 löscht den Mitarbeiter mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /mitarbeiter/delete
	 * 
	 * @return Redirect zurück zur Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /mitarbeiter/delete mit id " + id);
		
		Long longId = Long.parseLong(id);
		kontaktpersonRepo.deleteKontaktperson(longId);
		mitarbeiterRepo.delete(longId);
		response.redirect("/mitarbeiter");
		return null;
	}
}


