package webcontroller;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import object.Abteilung;
import spark.Request;

class AbteilungWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(AbteilungWebHelper.class);
	
	public static Abteilung abteilungFromWeb(Request request)
	{
		return new Abteilung(
				Long.parseLong(request.queryParams("abteilungDetail.abteilungid")),
				request.queryParams("abteilungDetail.abteilung"),
				request.queryParams("abteilungDetail.gueltigkeit"));
	}

}
