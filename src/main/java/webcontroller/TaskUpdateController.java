package webcontroller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import object.Task;
import repository.TaskRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Tasks
 * @author Dominic Bühler
 * @author Stéphane Krebs
 */

public class TaskUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(TaskUpdateController.class);
		
	private TaskRepository taskRepo = new TaskRepository();


	/**
	 * Schreibt das geänderte Task zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/task) mit der Task-id als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /task/update
	 * 
	 * @return redirect nach /task: via Browser wird /task aufgerufen, also editTask weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Task taskDetail = TaskWebHelper.taskFromWeb(request);
		
		log.trace("POST /task/update mit taskDetail " + taskDetail);
		
		//Redirect direkt auf /task
		//response.redirect("/task?id="+taskDetail.getTaskid()); --> wird nicht benötigt
		taskRepo.save(taskDetail);
		response.redirect("/task");
		return null;
	}
}


