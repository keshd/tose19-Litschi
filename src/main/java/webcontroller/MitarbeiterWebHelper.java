package webcontroller;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import object.Mitarbeiter;
import spark.Request;

class MitarbeiterWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(MitarbeiterWebHelper.class);
	
	public static Mitarbeiter mitarbeiterFromWeb(Request request)
	{
		return new Mitarbeiter(
				Long.parseLong(request.queryParams("mitarbeiterDetail.mitarbeiterid")),
				request.queryParams("mitarbeiterDetail.vorname"),
				request.queryParams("mitarbeiterDetail.nachname"),
				Boolean.parseBoolean(request.queryParams("mitarbeiterDetail.gueltigkeit")),
				Long.parseLong(request.queryParams("mitarbeiterDetail.abteilungsid")),
				request.queryParams("mitarbeiterDetail.geburtsdatum"));
	}

}
