package webcontroller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import object.*;
import repository.AbteilungRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelne Abteilung
 * @author Dominic Bühler
 */

public class AbteilungUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(AbteilungUpdateController.class);
		
	private AbteilungRepository abteilungRepo = new AbteilungRepository();
	


	/**
	 * Schreibt das geänderte Abteilung zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/Abteilung) mit der Abteilung-id als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /Abteilung/update
	 * 
	 * @return redirect nach /Abteilung: via Browser wird /Abteilung aufgerufen, also editAbteilung weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Abteilung abteilungDetail = AbteilungWebHelper.abteilungFromWeb(request);
		
		log.trace("POST /abteilung/update mit abteilungDetail " + abteilungDetail);
		
		//Redirect auf /Abteilung
		abteilungRepo.save(abteilungDetail);
		response.redirect("/abteilung");
		return null;
	}
}


