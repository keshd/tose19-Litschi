package webcontroller;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import object.*;
import repository.KontaktpersonRepository;
import repository.MitarbeiterRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelne Kontaktpersonen
 * @author Dominic Bühler
 * @author Stéphane Krebs
 */

public class KontaktpersonCreateController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(KontaktpersonCreateController.class);
	
	
	private KontaktpersonRepository kontaktpersonRepo = new KontaktpersonRepository();
	
	
	/**
	 * Requesthandler zum Bearbeiten einer Kontaktperson. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars eine neue Kontaktperson erstellt (Aufruf von /noftallliste/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars die Kontaktperson mit der übergebenen id upgedated (Aufruf /Notfallliste/save)
	 * Hört auf GET /Notfallliste
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "kontaktpersonDetailTemplate" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
		MitarbeiterRepository mitarbeiter = new MitarbeiterRepository();
				
		//TODO: check if 0 or null
		if(null == idString)
		{
			log.trace("GET /noftallliste für INSERT mit id " + idString);
			//der Submit-Button ruft /Notfallliste/new auf --> INSERT
			model.put("postAction", "/notfallliste/new");
			model.put("kontaktpersonDetail", new Kontaktperson());
			model.put("mitarbeiterDetail", mitarbeiter.getAll());

		}
		else
		{
			log.trace("GET /notfallliste für UPDATE mit id " + idString);
			//der Submit-Button ruft /notfallliste/update auf --> UPDATE
			model.put("postAction", "/notfallliste/update");
			model.put("mitarbeiterDetail", mitarbeiter.getAll());
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "kontaktpersonDetail" hinzugefügt. taskDetal muss dem im HTML-Template verwendeten Namen entsprechen 
			Long id = Long.parseLong(idString);
			Kontaktperson i = kontaktpersonRepo.getById(id);
			model.put("kontaktpersonDetail", i);
		}
		
		//das Template kontaktpersonDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "kontaktpersonCreateTemplate");
		}
		
}


