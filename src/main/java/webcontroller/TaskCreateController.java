package webcontroller;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import object.Task;
import repository.MitarbeiterRepository;
import repository.StatusRepository;
import repository.TaskRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Task
 * @author Dominic Bühler
 * @author Stéphane Krebs
 */

public class TaskCreateController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(TaskCreateController.class);
	
	
	private TaskRepository taskRepo = new TaskRepository();
	
	
	/**
	 * Requesthandler zum Bearbeiten eines Tasks. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neues Task erstellt (Aufruf von /task/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars das Task mit der übergebenen id upgedated (Aufruf /task/save)
	 * Hört auf GET /Task
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "taskDetailTemplate" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		MitarbeiterRepository mitarbeiter = new MitarbeiterRepository();
		StatusRepository status = new StatusRepository();
		
		
		HashMap<String, Object> model = new HashMap<String, Object>();
				
		//TODO: check if 0 or null
		if(null == idString)
		{
			log.trace("GET /task für INSERT mit id " + idString);
			//der Submit-Button ruft /task/new auf --> INSERT
			model.put("postAction", "/task/new");
			model.put("taskDetail", new Task());
			model.put("mitarbeiterDetail", mitarbeiter.getAll());
			model.put("statusDetail", status.getAll());

		}
		else
		{
			log.trace("GET /task für UPDATE mit id " + idString);
			//der Submit-Button ruft /task/update auf --> UPDATE
			model.put("postAction", "/task/update");
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "taskDetail" hinzugefügt. taskDetal muss dem im HTML-Template verwendeten Namen entsprechen 
			Long id = Long.parseLong(idString);
			Task i = taskRepo.getById(id);
			model.put("taskDetail", i);
			model.put("mitarbeiterDetail", mitarbeiter.getAll());
			model.put("statusDetail", status.getAll());
		}
		
		//das Template taskDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "taskCreateTemplate");
		}
	
	
	
}


