package webcontroller;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import repository.MitarbeiterRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * WWW-Controller
 * Liefert unter "/" die ganze Liste
 * 
 * @author Dominic Bühler
 * @author Stéphane Krebs
 */
public class MitarbeiterListController implements TemplateViewRoute {
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(MitarbeiterListController.class);

	MitarbeiterRepository repository = new MitarbeiterRepository();

	/**
	 *Liefert die Liste als Root-Seite "/mitarbeiter/list" zurück 
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		
		//Mitarbeiter werden geladen und die Collection dann für das Template unter dem namen "list" bereitgestellt
		//Das Template muss dann auch den Namen "list" verwenden.
		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("list", repository.getAll()); 	
		return new ModelAndView(model, "MitarbeiterListTemplate");
	}
}
