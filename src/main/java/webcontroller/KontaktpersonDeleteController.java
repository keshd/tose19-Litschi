package webcontroller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import repository.KontaktpersonRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Tasks
 * @author Dominic Bühler
 * @author Stéphane Krebs
 *
 */

public class KontaktpersonDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(KontaktpersonDeleteController.class);
		
	private KontaktpersonRepository kontaktpersonRepo = new KontaktpersonRepository();
	

	/**
	 * Löscht die Kontaktperson mit der übergebenen id in der Datenbank
	 * /notfalliste/delete&id=987 löscht das Task mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /notfallliste/delete
	 * 
	 * @return Redirect zurück zur Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /kontaktperson/delete mit id " + id);
		
		Long longId = Long.parseLong(id);
		kontaktpersonRepo.delete(longId);
		response.redirect("/notfallliste");
		return null;
	}
}


