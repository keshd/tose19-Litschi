package webcontroller;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import object.Task;
import spark.Request;

class TaskWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(TaskWebHelper.class);
	
	public static Task taskFromWeb(Request request)
	{
		return new Task(
				Long.parseLong(request.queryParams("taskDetail.taskid")),
				request.queryParams("taskDetail.titel"),
				request.queryParams("taskDetail.beschreibung"),
				Long.parseLong(request.queryParams("taskDetail.mitarbeiterid")),
				request.queryParams("taskDetail.start"),
				request.queryParams("taskDetail.ende"),
				Integer.parseInt(request.queryParams("taskDetail.status")));
	}

}
