package webcontroller;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import object.*;
import repository.AbteilungRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * WWW-Controller
 * Liefert unter "/Abteilung" die ganze Liste
 * 
 * @author Dominic Bühler
 * @author Stéphane Krebs
 */
public class AbteilungListController implements TemplateViewRoute {
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(AbteilungListController.class);

	AbteilungRepository repository = new AbteilungRepository();

	/**
	 *Liefert die Liste als Root-Seite "/abteilung/list" zurück 
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		
		//Abteilungen werden geladen und die Collection dann für das Template unter dem namen "list" bereitgestellt
		//Das Template muss dann auch den Namen "list" verwenden.
		HashMap<String, Collection<Abteilung>> model = new HashMap<String, Collection<Abteilung>>();
		model.put("list", repository.getAll());
		return new ModelAndView(model, "abteilungListTemplate");
	}
}
