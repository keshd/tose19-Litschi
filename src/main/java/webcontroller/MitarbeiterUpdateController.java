package webcontroller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import object.Mitarbeiter;
import repository.MitarbeiterRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Mitarbeiter
 * @author Dominic Bühler
 * @author Stéphane Krebs
 */

public class MitarbeiterUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(MitarbeiterUpdateController.class);
		
	private MitarbeiterRepository mitarbeiterRepo = new MitarbeiterRepository();


	/**
	 * Schreibt das geänderte Task zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/mitarbeiter) mit der mitarbeiter-id als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /mitarbeiter/update
	 * 
	 * @return redirect nach /mitarbeiter: via Browser wird /mitarbeiter aufgerufen, also edit mitarbeiter weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Mitarbeiter mitarbeiterDetail = MitarbeiterWebHelper.mitarbeiterFromWeb(request);
		
		log.trace("POST /mitarbeiter/update mit mitarbeiterDetail " + mitarbeiterDetail);
		//response.redirect("/mitarbeiter?id="+mitarbeiterDetail.getMitarbeiterid()); --> nicht verwendet
		//Automatischer Redirect auf /mitarbeiter
		mitarbeiterRepo.save(mitarbeiterDetail);		
		response.redirect("/mitarbeiter");
		return null;
	}
}
