package webcontroller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import object.Kontaktperson;
import repository.KontaktpersonRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Kontaktperson
 * @author Dominic Bühler
 * @author Stéphane Krebs
 */

public class KontaktpersonUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(KontaktpersonUpdateController.class);
		
	private KontaktpersonRepository kontaktpersonRepo = new KontaktpersonRepository();
	


	/**
	 * Schreibt die geänderte Kontaktperson zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/notfallliste) mit der Kontaktperson-id als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /notfallliste/update
	 * 
	 * @return redirect nach /notfallliste: via Browser wird /notfallliste aufgerufen, also editKontaktperson weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Kontaktperson kontaktpersonDetail = KontaktpersonWebHelper.kontaktpersonFromWeb(request);
		
		log.trace("POST /Kontaktperson/update mit kontaktpersonDetail " + kontaktpersonDetail);
		
		//Speichern der Kontaktperson in dann den Parameter für den Redirect abfüllen
		kontaktpersonRepo.save(kontaktpersonDetail);
		response.redirect("/notfallliste");
		return null;
	}
}


