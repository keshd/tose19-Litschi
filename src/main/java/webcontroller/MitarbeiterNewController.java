package webcontroller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import object.Mitarbeiter;
import repository.MitarbeiterRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Mitarbeiters
 * @author Dominic Bühler
 * @author Stéphane Krebs
 */

public class MitarbeiterNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(MitarbeiterNewController.class);
		
	private MitarbeiterRepository mitarbeiterRepo = new MitarbeiterRepository();
	
	/**
	 * Erstellt ein neuer Mitarbeiter in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /mitarbeiter&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /mitarbeiter/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Mitarbeiter mitarbeiterDetail = MitarbeiterWebHelper.mitarbeiterFromWeb(request);
		log.trace("POST /mitarbeiter/new mit mitarbeiterDetail " + mitarbeiterDetail);
		
		//insert gibt die von der DB erstellte id zurück.
		long id = mitarbeiterRepo.insert(mitarbeiterDetail);

		System.out.println("Neuer Mitarbeiter mit der ID = "+id+" erstellt");
		
		//die neue Id wird dem Redirect als Parameter nicht benötigt
		//der redirect erfolgt dann auf /mitarbeiter
		response.redirect("/mitarbeiter");
		return null;
	}
}


