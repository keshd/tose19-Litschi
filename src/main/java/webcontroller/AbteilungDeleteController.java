package webcontroller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import repository.AbteilungRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelne Abteilungen
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Dominic Bühler
 * @author Stéphane Krebs
 */

public class AbteilungDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(AbteilungDeleteController.class);
		
	private AbteilungRepository abteilungRepo = new AbteilungRepository();
	

	/**
	 * Löscht die Abteilung mit der übergebenen id in der Datenbank
	 * /Abteilung/delete&id=987 löscht das Abteilung mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /Abteilung/delete
	 * 
	 * @return Redirect zurück zur Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /abteilung/delete mit id " + id);
		
		Long longId = Long.parseLong(id);
		abteilungRepo.delete(longId);
		response.redirect("/abteilung");
		return null;
	}
}


