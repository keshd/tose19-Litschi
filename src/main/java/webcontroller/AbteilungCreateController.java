package webcontroller;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import object.*;
import repository.AbteilungRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelne Abteilungen
 * @author Dominic Bühler
 * @author Stéphane Krebs
 */

public class AbteilungCreateController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(AbteilungCreateController.class);
	
	private AbteilungRepository abteilungRepo = new AbteilungRepository();
	
	
	/**
	 * Requesthandler zum Bearbeiten einer Abteilung. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neues abteilung erstellt (Aufruf von /abteilung/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars das abteilung mit der übergebenen id upgedated (Aufruf /Abteilung/save)
	 * Hört auf GET /Abteilung
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "abteilungDetailTemplate" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
				
		//TODO: check if 0 or null
		if(null == idString)
		{
			log.trace("GET /abteilung für INSERT mit id " + idString);
			//der Submit-Button ruft /Abteilung/new auf --> INSERT
			model.put("postAction", "/abteilung/new");
			model.put("abteilungDetail", new Abteilung());

		}
		else
		{
			log.trace("GET /abteilung für UPDATE mit id " + idString);
			//der Submit-Button ruft /abteilung/update auf --> UPDATE
			model.put("postAction", "/abteilung/update");
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "abtilungDetail" hinzugefügt. abteilungDetail muss dem im HTML-Template verwendeten Namen entsprechen 
			Long id = Long.parseLong(idString);
			Abteilung i = abteilungRepo.getById(id);
			model.put("abteilungDetail", i);
		}
		
		//das Template abteilungDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "abteilungCreateTemplate");
		}
	
	
	
}


