package webcontroller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import object.Task;
import repository.TaskRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Tasks
 * @author Dominic Bühler
 *
 */

public class TaskNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(TaskNewController.class);
		
	private TaskRepository taskRepo = new TaskRepository();
	
	/**
	 * Erstellt ein neues Task in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /Task&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /task/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Task taskDetail = TaskWebHelper.taskFromWeb(request);
		log.trace("POST /task/new mit taskDetail " + taskDetail);
		
		//insert gibt die von der DB erstellte id zurück.
		long id = taskRepo.insert(taskDetail);

		System.out.println("Neuer Task mit der ID = "+id+" erstellt");
		
		//die neue Id wird dem Redirect als Parameter nicht benötigt
		//der redirect erfolgt dann auf /task
		response.redirect("/task");
		return null;
	}
}


