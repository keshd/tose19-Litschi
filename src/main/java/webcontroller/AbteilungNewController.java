package webcontroller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import object.*;
import repository.AbteilungRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Abteilungs
 * @author Dominic Bühler
 * @author Stéphane Krebs
 */

public class AbteilungNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(AbteilungNewController.class);
		
	private AbteilungRepository abteilungRepo = new AbteilungRepository();
	
	/**
	 * Erstellt ein neues Abteilung in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /Abteilung&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /abteilung/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Abteilung abteilungDetail = AbteilungWebHelper.abteilungFromWeb(request);
		log.trace("POST /abteilung/new mit abteilungDetail " + abteilungDetail);
		
		//insert gibt die von der DB erstellte id zurück.
		long id = abteilungRepo.insert(abteilungDetail);

		System.out.println("Neue Abteilung mit der ID = "+id+" erstellt");
				
		//die neue Id wird dem Redirect als Parameter nicht benötigt
		//der redirect erfolgt dann auf /abteilung
		response.redirect("/abteilung");
		return null;
	}
}


