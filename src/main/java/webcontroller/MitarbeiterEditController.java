package webcontroller;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import object.Mitarbeiter;
import repository.AbteilungRepository;
import repository.MitarbeiterRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Tasks
 * @author Dominic Bühler
 * @author Stéphane Krebs
 */

public class MitarbeiterEditController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(MitarbeiterEditController.class);
	
	
	private MitarbeiterRepository mitarbeiterRepo = new MitarbeiterRepository();
	
	
	/**
	 * Requesthandler zum Bearbeiten eines Mitarbeiters. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neues Task erstellt (Aufruf von /mitarbeiter/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars das Task mit der übergebenen id upgedated (Aufruf /mitarbeiter/save)
	 * Hört auf GET /Task
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "mitarbeiterDetailTemplate" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
		AbteilungRepository abteilung = new AbteilungRepository();
				
		//TODO: check if 0 or null
		if(null == idString)
		{
			log.trace("GET /mitarbeiter für INSERT mit id " + idString);
			//der Submit-Button ruft /mitarbeiter/new auf --> INSERT
			model.put("postAction", "/mitarbeiter/new");
			model.put("mitarbeiterDetail", new Mitarbeiter());
			model.put("abteilungDetail", abteilung.getAll());

		}
		else
		{
			log.trace("GET /mitarbeiter für UPDATE mit id " + idString);
			//der Submit-Button ruft /mitarbeiter/update auf --> UPDATE
			model.put("postAction", "/mitarbeiter/update");
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "mitarbeiterDetail" hinzugefügt. mitarbeiterDetail muss dem im HTML-Template verwendeten Namen entsprechen 
			Long id = Long.parseLong(idString);
			Mitarbeiter i = mitarbeiterRepo.getById(id);
			model.put("mitarbeiterDetail", i);
			model.put("abteilungDetail", abteilung.getAll());
		}
		
		//das Template mitarbeiterDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "MitarbeiterDetailTemplate");
		}
	
	
	
}


