package webcontroller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import repository.TaskRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Tasks
 * @author Dominic Bühler
 * @author Stéphane Krebs
 */

public class TaskDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(TaskDeleteController.class);
		
	private TaskRepository taskRepo = new TaskRepository();
	

	/**
	 * Löscht das Task mit der übergebenen id in der Datenbank
	 * /task/delete&id=987 löscht das Task mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /task/delete (besser wäre POST)
	 * 
	 * @return Redirect zurück zur Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /task/delete mit id " + id);
		
		Long longId = Long.parseLong(id);
		taskRepo.delete(longId);
		response.redirect("/task");
		return null;
	}
}


